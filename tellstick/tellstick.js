module.exports = function(RED) {
	"use strict";
    var telldus = require('telldus');

    function TellstickNode(config) {
        RED.nodes.createNode(this,config);
        this.deviceId = config.deviceId;
        this.option = config.option;
        var node = this;

        node.on('input', function(msg) {
            var option = node.option;
            var deviceId = parseInt(node.deviceId);

            switch(option) {
                case 'ON':
                    telldus.turnOn(deviceId, function(err) {
                        if(!err)
                            node.send({payload:"Turned on device "+deviceId});
                    });
                    break;
                case 'OFF':
                    telldus.turnOff(deviceId, function(err) {
                        if(!err)
                            node.send({payload:"Turned off device "+deviceId});
                    });
                    break;
                default:
                    node.send({payload:"Unknown option "+option+" for deivce id"+deviceId});
                    break;
            }
        });

        node.on('close', function(){
        });

        node.on('timeout', function(){
        });
    }
    RED.nodes.registerType("tellstick",TellstickNode);
}